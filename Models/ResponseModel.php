<?php

/**
 * Modelo de la respuesta
 * 
 * @property int $status Código del status. Default 200 = Todo correcto
 * @property array $data Información adicional
 */

class ResponseModel
{

  public $status;
  public $data;

  function __construct(int $status = 200, array $data = [])
  {
    $this->status = $status;
    $this->data = $data;
  }
}
