<?php

require_once "Classes/ActiveRecord.php";

class ReservationModel extends ActiveRecord
{ 

    public $id;
    public $older;
    public $younger;
    public $user_id;
    public $room_id;
    public $start_date;
    public $end_date;
    public $total;
    public $canceled;

    function __construct($con, $model = null)
    {
        $this->con = $con;
        $this->table = "reservations";
        if ($model) {
            $this->setReservation($model);
        }
    }

    private function setReservation($data): void
    {
        $this->id = $this->intval($data->id ?? null);
        $this->older =  $this->intval($data->older ?? null);
        $this->younger =  $this->intval($data->younger ?? null);
        $this->user_id =  $this->intval($data->user_id ?? null);
        $this->room_id =  $this->intval($data->room_id ?? null);
        $this->start_date =  $this->strval($data->start_date ?? null);
        $this->end_date =  $this->strval($data->end_date ?? null);
        $this->canceled =  $this->intval($data->canceled ?? null);
        $this->total =  $this->intval($data->total ?? null);
    }

    public function getAll(){
        return $this->queryAll("Select * from {$this->table} re  
        INNER JOIN rooms r ON r.id = re.room_id 
        INNER JOIN type_rooms tr ON r.type_id = tr.id WHERE  canceled <> 0 ");
    }

    public function getByUserId($id){
        return $this->queryAll("Select *, r.name, tr.cost, tr.type from {$this->table}  re  
        INNER JOIN rooms r ON r.id = re.room_id 
        INNER JOIN type_rooms tr ON r.type_id = tr.id 
        WHERE user_id = ? and canceled <> 0", [$id]);
    }

    public function addReservation(): int
    {
        try {

            $SQL = "INSERT INTO {$this->table} (
                 older,
                younger,
                user_id,
                room_id,
                start_date,
                end_date,
                canceled,
                total
            ) VALUES (
                :older,
                :younger,
                :room_id,
                :user_id,
                :start_date,
                :end_date,
                :total
            )";
            $params = array(
                ':older' =>  $this->older,
                ':younger' =>  $this->younger,
                ':room_id' =>  $this->room_id,
                ':user_id' => $this->user_id,
                ':start_date' =>  $this->start_date,
                ':end_date' =>  $this->end_date,
                ':total' => $this->total,
            );

            return $this->insert($SQL, $params);
        } catch (\PDOException $e) {
            error_log('createRoom error: ' . $e->getMessage());
            echo $e->getMessage();
            return 0;
        }
    }

    /**
     * Obtiene una reservacion por su id
     * @param int $id id de la reservacion
     */
    public function getById(int $id): void
    {
        $model = $this->find($id);
        if ($model) {
            $this->setReservation($model);
        }
    }
    
        /**
     * Actualiza la reservacion en bd
     * @return bool $update reservacion actualizada
     */
    public function updateReservation(): bool
    {
        try {

            $SQL = "UPDATE {$this->table} SET  
                ':older' = older,
                ':younger' = younger,
                ':room_id' = room_id,
                :user_id = user_id,
                ':start_date' = start_date,
                ':end_date' = end_date,
                WHERE id = :id";
            $params = array(
                ':older' =>  $this->older,
                ':younger' =>  $this->younger,
                ':room_id' =>  $this->room_id,
                ':user_id' => $this->user_id,
                ':start_date' =>  $this->start_date,
                ':end_date' =>  $this->end_date,
                ':id' => $this->id
            );
            $this->update($SQL, $params);

            return true;
        } catch (\PDOException $e) {
            error_log('updateReservation error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Elimina la reservacion en bd
     * @return bool $delete reservacion eliminada
     */
    public function deleteReservation(): bool
    {
        try {

            $SQL = "DELETE FROM {$this->table} WHERE id = :id";

            $params = array(
                ':id' => $this->id
            );

            $this->delete($SQL, $params);
            return true;
        } catch (\PDOException $e) {
            error_log('deleteReservation error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    public function getReservationsIdsFromDates(){
        $SQL = "SELECT r.id FROM reservations re
        INNER JOIN rooms r ON r.id = re.room_id
        WHERE re.start_date > ? OR re.end_date < ? ;";
        return $this->queryAll($SQL, [$this->start_date, $this->end_date]);
    }



            /**
     * Actualiza la reservacion en bd
     * @return bool $update reservacion actualizada
     */
    public function cancelReservation(): bool
    {
        try {

            $SQL = "UPDATE {$this->table} SET  
                ':canceled' = canceled,
                WHERE id = :id";
            $params = array(
                ':canceled' =>  $this->canceled,
                ':id' => $this->id
            );
            $this->update($SQL, $params);

            return true;
        } catch (\PDOException $e) {
            error_log('updateReservation error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }
}
