<?php

require_once "Classes/ActiveRecord.php";

class RoomModel extends ActiveRecord
{

    public $id;
    public $name;
    public $type_id;

    function __construct($con, $model = null)
    {
        $this->con = $con;
        $this->table = "rooms";
        if ($model) {
            $this->setRoom($model);
        }
    }

    private function setRoom($data): void
    {
        $this->id = $this->intval($data->id ?? null);
        $this->name =  $this->strval($data->name ?? null);
        $this->type_id =  $this->intval($data->type_id ?? null);
    }


    public function getAll()
    {
        return $this->queryAll("Select r.*, tr.type, tr.cost from {$this->table} r INNER JOIN type_rooms tr ON r.type_id = tr.id");
    }

    public function executeCustomQuery($SQL)
    {
        return $this->queryAll($SQL);
    }

    public function addRoom(): int
    {
        try {

            $SQL = "INSERT INTO {$this->table} (
                name,
                type_id
            ) VALUES (
              :name,
              :type_id
            )";

            $params = array(
                ':name' => $this->name,
                ':type_id' =>  $this->type_id,
            );

            return $this->insert($SQL, $params);
        } catch (\PDOException $e) {
            error_log('createRoom error: ' . $e->getMessage());
            echo $e->getMessage();
            return 0;
        }
    }

    /**
     * Obtiene una habtacion por su id
     * @param int $id id de la habtacion
     */
    public function getById(int $id): void
    {
        $model = $this->find($id);
        if ($model) {
            $this->setRoom($model);
        }
    }

    /**
     * Actualiza la habtacion en bd
     * @return bool $update habtacion actualizada
     */
    public function updateRoom(): bool
    {
        try {

            $SQL = "UPDATE {$this->table} SET  name = :name, type_id = :type_id WHERE id = :id";
            $params = array(
                ':name' => $this->name,
                ':type_id' => $this->type_id,
                ':id' => $this->id,
            );
            $this->update($SQL, $params);

            return true;
        } catch (\PDOException $e) {
            error_log('updateRoom error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Elimina el habtacion en bd
     * @return bool $delete habtacion eliminado
     */
    public function deleteRoom(): bool
    {
        try {

            $SQL = "DELETE FROM {$this->table} WHERE id = :id";

            $params = array(
                ':id' => $this->id
            );

            $this->delete($SQL, $params);
            return true;
        } catch (\PDOException $e) {
            error_log('deleteRoom error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    public function getRoomsWithoutIds(array $ids)
    {
        $SQL = "SELECT r.*, tr.type, tr.cost FROM rooms r INNER JOIN type_rooms tr ON r.type_id = tr.id";
        $params = array();
        if (sizeof($ids) > 0) {
            $index = 0;
            $SQL .= " WHERE r.id NOT IN( ";
            foreach ($ids as  $id) {
                if ($index == 0) {
                    $SQL .= " ?";
                } else {
                    $SQL .= ", ?";
                }
                array_push($params,$id->id );
                $index++;
            }
            $SQL .= " );";
        }

        return $this->queryAll($SQL, $params);
    }
}
