<?php

require_once "Classes/ActiveRecord.php";

class AuthModel extends ActiveRecord
{

    public $id;
    public $username;
    public $email;
    public $password;
    public $name;
    public $apt_pat;
    public $apt_mat;
    public $phone;
    public $street;
    public $num_int;
    public $num;
    public $col;
    public $curp;
    public $clave;
    public $isAdmin;

    function __construct($con, $model = null)
    {
        $this->con = $con;
        $this->table = "users";
        if ($model) {
            $this->setAuth($model);
        }
    }

    private function setAuth($data): void
    {
        $this->id = $this->intval($data->id ?? null);
        $this->username =  $this->strval($data->username ?? null);
        $this->email =  $this->strval($data->email ?? null);
        $this->password =  $this->strval($data->password ?? null);
        $this->name =  $this->strval($data->name ?? null);
        $this->apt_pat =  $this->strval($data->apt_pat ?? null);
        $this->apt_mat =  $this->strval($data->apt_mat ?? null);
        $this->phone =  $this->intval($data->phone ?? null);
        $this->street =  $this->strval($data->street ?? null);
        $this->num_int =  $this->intval($data->num_int ?? null);
        $this->num =  $this->intval($data->num ?? null);
        $this->col =  $this->intval($data->col ?? null);
        $this->curp =  $this->strval($data->curp ?? null);
        $this->clave =  $this->intval($data->clave ?? null);
        $this->isAdmin =  $this->intval($data->isAdmin ?? null);
        
    }

    public function register(): int
    {
        try {

            $SQL = "INSERT INTO {$this->table} (
                username,
                password,
                email
                name,
                apt_pat,
                apt_mat,
                phone,
                street,
                num_int,
                num,
                col,
                curp,
                clave
            ) VALUES (
              :username,
              :password,
              :email,
              :name,
              :apt_pat,
              :apt_mat,
              :phone,
              :street,
              :num_int,
              :num,
              :col,
              :curp,
              :clave
            )";


            $params = array(
                ':username' => $this->username,
                ':password' => $this->password,
                ':email' => strtolower($this->email),
                ':name' => $this->name,
                ':apt_pat' => $this->apt_pat,
                ':apt_mat' => $this->apt_mat,
                ':phone' => $this->phone,
                ':street' => $this->street,
                ':num_int' => $this->num_int,
                ':num' => $this->num,
                ':col' => $this->col,
                ':curp' => $this->curp,
                ':clave' => $this->clave,
            );


            return $this->insert($SQL, $params);
        } catch (\PDOException $e) {
            error_log('createUser error: ' . $e->getMessage());
            echo $e->getMessage();
            return 0;
        }
    }


    /**
     * Obtiene un usuario por su id
     * @param int $id id del usuario
     */
    public function getById(int $id): void
    {
        $model = $this->find($id);
        if ($model) {
            $this->setAuth($model);
        }
    }

    public function getByUsernameAndPassword()
    {
        $SQL = "SELECT * {$this->table} WHERE username = :username";
        return  $this->queryAll($SQL, array(':username' => $this->username));
    }

    /**
     * Actualiza el usuario en bd
     * @return bool $update usuario actualizado
     */
    public function updateUser(): bool
    {
        try {

            $SQL = "UPDATE {$this->table} 
                SET username = :username, 
                password = :password,
                email = :email,
                name = :name,
                apt_pat = :apt_pat,
                apt_mat = apt_mat,
                phone = :phone,
                street = :street,
                num_int = :num_int,
                num = :num,
                col = :col,
                curp = :curp,
                clave = :clave
                WHERE id = :id";
            $params = array(
                ':username' => $this->username,
                ':password' => $this->password,
                ':email' => strtolower($this->email),
                ':name' => $this->name,
                ':apt_pat' => $this->apt_pat,
                ':apt_mat' => $this->apt_mat,
                ':phone' => $this->phone,
                ':street' => $this->street,
                ':num_int' => $this->num_int,
                ':num' => $this->num,
                ':col' => $this->col,
                ':curp' => $this->curp,
                ':clave' => $this->clave,
            );
            $this->update($SQL, $params);

            return true;
        } catch (\PDOException $e) {
            error_log('updateUser error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Elimina el usuario en bd
     * @return bool $delete usuario eliminado
     */
    public function deleteUser(): bool
    {
        try {

            $SQL = "DELETE FROM {$this->table} WHERE id = :id";

            $params = array(
                ':id' => $this->id
            );

            $this->delete($SQL, $params);
            return true;
        } catch (\PDOException $e) {
            error_log('deleteUser error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }
}
