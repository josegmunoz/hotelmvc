<?php

require_once "Classes/ActiveRecord.php";

class TypeRoomModel extends ActiveRecord
{

    public $id;
    public $type;
    public $cost;

    function __construct($con, $model = null)
    {
        $this->con = $con;
        $this->table = "type_rooms";
        if ($model) {
            $this->setRoom($model);
        }
    }

    private function setRoom($data): void
    {
        $this->id = $this->intval($data->id ?? null);
        $this->type =  $this->strval($data->type ?? null);
        $this->cost =  $this->intval($data->cost ?? null);
    }


    public function getAll()
    {
        return $this->queryAll("Select * from {$this->table}");
    }

    public function addType(): int
    {
        try {

            $SQL = "INSERT INTO {$this->table} (
                type,
                cost
            ) VALUES (
              :type,
              :cost
            )";

            $params = array(
                ':type' => $this->type,
                ':cost' =>  $this->cost,
            );
            return $this->insert($SQL, $params);
        } catch (\PDOException $e) {
            error_log('createRoom error: ' . $e->getMessage());
            echo $e->getMessage();
            return 0;
        }
    }

    /**
     * Obtiene un typo por su id
     * @param int $id id del typo
     */
    public function getById(int $id): void
    {
        $model = $this->find($id);
        if ($model) {
            $this->setRoom($model);
        }
    }

    /**
     * Actualiza el typo en bd
     * @return bool $update typo actualizado
     */
    public function updateTypeRoom(): bool
    {
        try {

            $SQL = "UPDATE {$this->table} SET  type = :type, cost = :cost WHERE id = :id";
            $params = array(
                ':type' => $this->type,
                ':cost' => $this->cost,
                ':id' => $this->id,
            );
            $this->update($SQL, $params);

            return true;
        } catch (\PDOException $e) {
            error_log('updateUser error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Elimina el typo en bd
     * @return bool $delete typo eliminado
     */
    public function deleteRoomType(): bool
    {
        try {

            $SQL = "DELETE FROM {$this->table} WHERE id = :id";

            $params = array(
                ':id' => $this->id
            );

            $this->delete($SQL, $params);
            return true;
        } catch (\PDOException $e) {
            error_log('deleteUser error: ' . $e->getMessage());
            echo $e->getMessage();
            return false;
        }
    }
}
