$(document).ready(function () {

    init();
    function init (){
        isAdmin();
    }

    function isAdmin() {
        var data = {
            action: "isAdmin",
            module: "Auth",
            data: {
                nothing: ""
            }
        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    getReservations("getReservation");
                } else {
                    getReservations("getReservationsByUserID");
                }
            },
            "json"
        );

    }

    function getReservations(action) {
        var data = {
            action: action,
            module: "Reservation",
            data: {
                nothing: "",
            }
        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    data.data.forEach(type => {
                        template += `
                        <div class="col-4">
                        <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="..." alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Habitacion ${type.name}</h5>
                            <p class="card-text">Tipo: ${type.type}  
                            <br> Costo Total: ${(type.cost)}
                            <br>
                            Adultos:${type.older} Niños:${type.younger}
                            </p>
                            <a href="#" roomid="${type.id}" class="btn btn-primary room-cancel">Cancelar Reservacion</a>
                                </div>
                            </div>
                        </div>`;
                    });
                } else {
                    template += `
                        <div class="col-12">
                        <div class="card">
                        <div class="card-body d-flex justify-content-center" >
                        ${data.data.name}
                        </div>
                        </div>
                        </div>`;
                }
                $('#cards').html(template);
            },
            "json"
        );
    }



    // Espera a que el item sea selecionado para abrir modal de update
    $(document).on('click', '.room-cancel', (e) => {
        const element = $(this)[0].activeElement;
        var data = {
            action: "cancelReservation",
            module: "Reservation",
            data: {
                canceled: 1,
            }
        }
        if (confirm("Desea cancelar la reservacion")) {
            $.post("/MVC/Router", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        window.location.href = "/Index";
                    }
                },
                "json"
            );
        }
        e.preventDefault();
    });

});