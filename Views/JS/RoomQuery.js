$(document).ready(function () {
    let id = 0;
    init();
    function init() {
        updateTable();
        chargeSelect('#select1');
    }

    $("#save").click(function (e) {
        e.preventDefault();
        var formdata = $('#addRoom').serializeArray();
        var data = {
            action: "createRoom",
            module: "Room",
            data: {
                name: formdata[0].value,
                type_id: $("#select1").val(),
            }
        }
        if (formdata[0].value == "" && formdata[0].value == "") {
            $("#alerts").css("visibility", "initial");
        } else {
            resetForm();
            $("#exampleModalCenter").modal('hide');
            $.post("/MVC/Router", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        updateTable();
                    }
                },
                "json"
            );
        }
    });


    $("#update").click(function (e) {
        e.preventDefault();
        var formdata = $('#updateRoom').serializeArray();
        var data = {
            action: "updateRoom",
            module: "Room",
            data: {
                id: id,
                name: formdata[0].value,
                type_id: $("#select2").val(),
            }
        }
        if (formdata[0].value == "" && formdata[0].value == "") {
            $("#alertupdate").css("visibility", "initial");
        } else {
            resetForm();
            $("#updateModal").modal('hide');
            $.post("/MVC/Router", data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        updateTable();
                    }
                },
                "json"
            );
        }
    });

    function updateTable() {
        var data = {
            action: "getRooms",
            module: "Room",
            data: {
                nothing: ""
            }
        }
        $.post("/MVC/Router", data,
            function (data, textStatus, jqXHR) {
                if(data.status == 200){
                    const rooms = data.data[0];
                    let template = '';
                    rooms.forEach(room => {
                        template += `
                        <tr roomId="${room.id}">
                        <td>${room.id}</td>
                        <td>
                        <a href="#" class="room-item">
                          ${room.name} 
                        </a>
                        </td>
                        <td>${room.type}</td>
                        <td>
                          <button class="room-delete btn btn-danger">
                           Eliminar
                          </button>
                        </td>
                        </tr>`;
                    });
                    $('#rooms').html(template);
                }
            },
            "json"
        );
    }

    function resetForm() {
        $('#addType').trigger("reset");
        $('#updateRoom').trigger("reset");
        $("#alerts").css("visibility", "hidden");
    }


    // Espera a que el item sea selecionado para abrir modal de update
    $(document).on('click', '.room-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const idroom = $(element).attr('roomId');
        var data = {
            action: "getRoomById",
            module: "Room",
            data: {
                id: idroom
            }
        }
        chargeSelect("#select2");
        $.post('/MVC/Router', data,
            function (data, textStatus, jqXHR) {
                const room = data.data[0];
                id = room.id;
                $('#uptname').val(room.name);
                $("#select2").val(room.type_id);
                $('#updateModal').modal('show');
            },
            "json"
        );
        e.preventDefault();
    });

    // Hace una peticion para eliminar el tipo
    $(document).on('click', '.room-delete', (e) => {
        if (confirm('Estas seguro de eliminarlo?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('roomId');
            var data = {
                action: "deleteRoom",
                module: "Room",
                data: {
                    id: id
                }
            }
            $.post('/MVC/Router', data,
                function (data, textStatus, jqXHR) {
                    if (data.status == 200) {
                        updateTable();
                    }
                },
                "json"
            );
        }
    });


    function chargeSelect(select) {
        var data = {
            action: "getRoomTypes",
            module: "TypeRoom",
            data: {
                id: ""
            }
        }
        $.post('/MVC/Router', data,
            function (data, textStatus, jqXHR) {
                if (data.status == 200) {
                    let rooms = data.data[0];
                    rooms.forEach(room => {
                        $(select).append(`<option value="${room.id}"> ${room.type}</option>`);
                    });
                }
            },
            "json"
        );
    }

});