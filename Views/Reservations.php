<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reservaciones</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/08cf087951.js" crossorigin="anonymous"></script>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Menu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/MVC/Views/Index">Menu Principal <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="logout">Cerrar Sesion</a>
      </li>
    </ul>
  </div>
</nav>
    <form >
        <div class="input-group mb-3">
            <input id="from" width="276" placeholder="Fecha Inicio" class="form-control" />
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> <i class="fas fa-calendar-alt"></i></span>
            </div>
        </div>
        <div class="input-group mb-3">
            <input id="to" width="276" placeholder="Fecha Fin" class="form-control" />
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> <i class="fas fa-calendar-alt"></i></span>
            </div>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Adultos</span>
            </div>
            <input type="number" name="older" value="1" min="1" max="99" placeholder="" class="form-control" id="older">
            <div class="input-group-prepend">
                <span class="input-group-text">Niños</span>
            </div>
            <input type="number" name="younger" value="0" min="0" max="99" placeholder="" class="form-control" id="younger">
        </div>

        <div class="input-group mb-3">
            <input id="save" type="submit" class="form-control btn btn-primary" />
        </div>
        <div class="row" id="cards">

        </div>
    </form>
    <script src="Js/ReservationQuery.js" type="text/javascript"> </script>
    <script src="Js/AuthQuerys.js" type="text/javascript"> </script>
</body>

</html>