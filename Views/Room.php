<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Habitaciones</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="forms.css">
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>

<body>
    <script src="Js/RoomQuery.js" type="text/javascript">  </script>
    <script src="Js/AuthQuerys.js" type="text/javascript"> </script>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/MVC/Views/Index">Menu Principal <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="/MVC/Views/Reservations">Regresar a Reservaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="logout">Cerrar Sesion</a>
            </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="jumbotron">
            <h1 class="display-4 d-flex justify-content-center"> Habitaciones</h1>
            <p class="lead d-flex justify-content-center">
                <div class="row">
                    <div class="col-3">

                    </div>
                    <div class="col-3">
                    </div>
                    <div class="col-3">
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                            Agregar Habitacion
                        </button>
                    </div>
                </div>
            </p>
            <hr class="my-4">
            <div class="row">
                <div class="col-12">
                    <table class="table ">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">tipo </th>
                                <th scope="col">Eliminar </th>
                            </tr>
                        </thead>
                        <tbody id="rooms"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar Nueva Habitacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger  d-flex justify-content-center" id="alerts" style="visibility: hidden;" role="alert">
                        Rellene Los Datos
                    </div>
                    <form id="addRoom">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Numero" aria-label="Nombre" name="name" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <select class="custom-select" id="select1" placeholder="Tipo De Habitacion" name="cost">
                                <option selected>Elegir Tipo De Habitacion...</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="save">Guardar Habitacion</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Actualizar Habitacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger  d-flex justify-content-center" id="alertupdate" style="visibility: hidden;" role="alertupdate">
                        Rellene Los Datos
                    </div>
                    <form id="updateRoom">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Numero" aria-label="Nombre" name="name" aria-describedby="basic-addon1" id="uptname">
                        </div>
                        <div class="input-group mb-3">
                            <select class="custom-select" id="select2" placeholder="Tipo De Habitacion" name="upttype_id">
                                <option selected>Elegir Tipo De Habitacion...</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="update">Guardar Habitacion</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>