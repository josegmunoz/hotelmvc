<?php

class Database extends \PDO
{
  /**
   * Contador del número de transacciones realizadas
   * @var int
   */
  protected $transactionCounter = 0;

  /**
   * Inicia una transacción en la que hacer querys sin que se vean refleadas hasta realizar un commit
   * @return bool Regresa si se pudo iniciar una transacción
   */
  function beginTransaction(): bool
  {
    if (!$this->transactionCounter++) {
      return parent::beginTransaction();
    }
    return $this->transactionCounter >= 0;
  }

  /**
   * Realiza un commit, guarda los cambios realizados durante la transacción en bd
   * @return bool Regresa si se pudo realizar el commit
   */
  function commit()
  {
    if (!--$this->transactionCounter) {
      return parent::commit();
    }
    return $this->transactionCounter >= 0;
  }

  /**
   * Realiza un rollback, revierte todos los cambios realizados durante la transacción y no se guardan en bd
   * @return bool Regresa si se pudo realizar el rollback
   */
  function rollback(): bool
  {
    if ($this->transactionCounter >= 0) {
      $this->transactionCounter = 0;
      return parent::rollback();
    }
    $this->transactionCounter = 0;
    return false;
  }

  /**
   * Genera la conexión con la bd
   * @param array $config Arreglo con la configuración para la conexión con la bd
   * @return Database|null $con Conexión realizada a la bd
   */
  public static function getConnection(array $config = []): Database
  {

    $connect = function ($dsn, $user, $pass) {

      if ($user == '')
        return null;

      try {
        $options = [
          \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
          \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
          \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $con = new Database($dsn, $user, $pass, $options);
        // error_log("return connection success user: ".$user);
        // echo 'return connection success user: '.$user.'<br>';
        return $con;
      } catch (\PDOException $e) {
        // echo 'return connection failed '.$user.': '.$e->getMessage().'<br>';
        error_log("return connection failed user: " . $user);
        return null;
      }
    };

    $user = $config['user'];
    $host = $config['host'];
    $db   = $config['db'];
    $charset = $config['charset'];
    $pass = $config['pass'];

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $session_user = isset($_SESSION['account']) ? $_SESSION['account'] : '';
    if ($session_user <> '') {
      $con = $connect($dsn, $session_user, $pass);
      if (!$con) {
        $con = $connect($dsn, $user, $pass);
      }
    } else {
      $con = $connect($dsn, $user, $pass);
    }

    return $con;
  }

  // public function getCurrentDBUser(){
  //   $res = $this->con->query("SELECT SUBSTRING_INDEX(USER(), '@', 1) as user");
  //   $dbUser = $res->fetchColumn();
  //   echo "dbuser: ".$dbUser;
  // }
}
