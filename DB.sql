create table users(
id int not null primary key auto_increment,
username varchar(100) not null,
password varchar(300) not null,
email varchar(200) not null,
name varchar(100) not null,
apt_pat varchar(100) ,
apt_mat varchar(100),
phone int not null,
street varchar(200) not null,
num int not null,
num_int int not null,
col varchar(100) not null,
curp varchar(25) not null,
clave varchar(100) not null,
isAdmin tinyint not null default 0);

create table type_rooms(
id int not null primary key auto_increment,
type varchar(50) not null,
cost decimal(4,2) not null);

create table rooms (
id int not null primary key auto_increment,
name varchar(45) not null,
type_id int not null,
foreign key (type_id) references type_rooms(id)
);

create table reservations (
id int not null primary key auto_increment,
older int not null,
younger int not null,
room_id int not null,
user_id int not null,
start_date date not null,
end_date date not null,
canceled tinyint not null default 0,
total decimal(4, 2) not null,
foreign key (room_id) references rooms(id),
foreign key (user_id) references users(id));
