<?php
require_once ("Controller.php");
require_once ("Models/ResponseModel.php");
require_once ("Models/AuthModel.php");

class AuthController extends Controller
{

    public function login($data): ResponseModel
    {
        //Destruye la sesión, en caso de que hubiera alguna anteriormente
        if (!(session_status() == PHP_SESSION_NONE)) {
            session_destroy();
            session_start();
        }
        $authModel = new AuthModel($this->con, $data);
        $authModel->getByUsernameAndPassword();

        if (!password_verify($authModel->password, $data->password)) {
            return new ResponseModel(201, array());
        } else {
            $_SESSION["user_id"] = $authModel->id;
            $_SESSION["username"] = $authModel->username;
            $_SESSION["email"] = $authModel->email;
            $_SESSION["password"] = $authModel->password;
        }

        return new ResponseModel(200, array());
    }


    public function register($data): ResponseModel
    {

        $data->password = password_hash($data->password, PASSWORD_BCRYPT, ['cost' => 11]);
        $authModel = new AuthModel($this->con, $data);
        $id = $authModel->register();

        if ($id == 0) {
            return new ResponseModel(202, [$data]);
        } else {
            $_SESSION["user_id"] = $id;
            $_SESSION["username"] = $data->username;
            $_SESSION["email"] = $data->email;
            $_SESSION["password"] = $data->password;
        }
        echo print_r($_SESSION);
        return new ResponseModel(200, [$data]);
    }

    public function isAdmin($data){
        $authModel = new AuthModel($this->con, $data);
        $id = $authModel->getById($_SESSION["user_id"]);
        if($id->isAdmin == 0){
            return new ResponseModel(201, []);
        } else {
            return new ResponseModel(200, []);
        }
    }

    public function logout($data){
        if (!(session_status() == PHP_SESSION_NONE)) {
            session_destroy();
            return  new ResponseModel(200, []);
        }
    }
}
