<?php
require_once "Controller.php";
require_once "Models/ReservationModel.php";
require_once "Models/ResponseModel.php";
require_once "Models/RoomModel.php";
require_once "Models/TypeRoomModel.php";

class RoomController extends Controller
{

    public function createRoom($data): ResponseModel
    {
        $roomController = new RoomModel($this->con, $data);

        $id = $roomController->addRoom();
        if ($id == 0) {
            return new ResponseModel(201, array());
        }
        return new ResponseModel(200, array());
    }

    public function getRooms($data)
    {
        $roomController = new RoomModel($this->con, $data);
        $items =  $roomController->getAll();

        if (sizeof($items) == 0) {
            return new ResponseModel(202, array());
        }
        return new ResponseModel(200, array($items));
    }

    public function getRoomById($data)
    {
        $roomController = new RoomModel($this->con, $data);
        $roomController->getById($data->id);
        return new ResponseModel(200, array($roomController));
    }

    public function updateRoom($data)
    {
        $roomController = new RoomModel($this->con, $data);
        $update =  $roomController->updateRoom();
        if (!$update) {
            return new ResponseModel(203, array());
        } else {
            return new ResponseModel(200, array());
        }
    }

    public function deleteRoom($data)
    {
        $roomController = new RoomModel($this->con, $data);
        $update =  $roomController->deleteRoom();
        if (!$update) {
            return new ResponseModel(204, array());
        } else {
            return new ResponseModel(200, array());
        }
    }

    public function getAllAviableRooms($data)
    {
        $roomController = new RoomModel($this->con, $data);
        $reservationController = new ReservationModel($this->con, $data);
        $ids = $reservationController->getReservationsIdsFromDates($data);

        $items = $roomController->getRoomsWithoutIds($ids);
        if (sizeof($items) == 0) {
            return new ResponseModel(206, ["name" => "No hay habitaciones disponibles"]);
        }
        return new ResponseModel(200, array($items[0]));
    }
}
