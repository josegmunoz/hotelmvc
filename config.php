<?php
return [
  'production' => false,
  'base_route' => '/MVC/php/',
  'db' => [
    'host' => 'localhost',
    'user' => 'root',
    'pass' => '',
    'db' => 'test',
    'charset' => 'utf8'
  ],
];