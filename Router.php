<?php
require_once "Classes/Database.class.php";
date_default_timezone_set('America/Mexico_City');

session_start();
$config = include('config.php');
// $post = json_decode(file_get_contents("php://input"));
$post = $_POST;
// echo print_r($_SESSION);
if (empty($_POST)) {
  $post = (object) $_POST;
}

if (!isset($post["action"])) {
  exit();
}

$action = $post["action"];
$moduleClass = $post["module"];
$data = json_decode(json_encode($post["data"]), FALSE);

//Crea una conexión a db para todas las peticiones
$connection = Database::getConnection($config['db']);

//Incluye el controller del módulo
include_once "Controllers/{$moduleClass}Controller.php";
$moduleController = $moduleClass."Controller";

$controller = new $moduleController($connection);

//Si sólo es de lectura
// if ($_SERVER['REQUEST_METHOD'] == 'GET'  || !(session_status() == PHP_SESSION_NONE)) {
  if ($_SERVER['REQUEST_METHOD'] == 'GET' ) {
  //Ejecuta la función solicitada
  return header("Views/Index");
} else {
  //Desactiva el autocommit
  $connection->beginTransaction();

  //Ejecuta la función solicitada
  $result = $controller->$action($data);

  //Si el resultado fue correcto, guarda los cambios en bd
  //Sino, regresa al punto anterior para no tener cambios a medias
  if ($result->status == 200)
    $connection->commit();
  else
    $connection->rollback();
}

//Regresa la respuesta en json
echo json_encode($result);
